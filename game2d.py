from direct.showbase.ShowBase import ShowBase
from panda3d.core import OrthographicLens
from direct.task import Task

class Game2D(ShowBase):
    """
    A class for 2D Games in Python.

    This class is a wrapper class around ShowBase class from Panda3D.
    Panda3D is a framework for 3D games and applications. Our 2D games use only 2D (x,y) coordinates.
    However, Panda3D only cares about 3D worlds, so we do the following conversion:

        Game2D horizontal (x) direction is Panda3D horizontal (x) direction
        Game2D vertical (y) direction is Panda3D vertical (z) direction

        Panda3D y direction is not used and points to our eyes.
    """

    # Default width and height for game windows
    DEFAULT_WIDTH = 800
    DEFAULT_HEIGHT = 600

    # Default depth range for visible objects
    NEAR_VAL = 0
    FAR_VAL = 1

    # Constants returned by gameLoop
    cont = Task.cont
    done = Task.done

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        """
        Construct a new Game2D object.


        Parameters
        ----------
            width: int
                width of the game window in pixels (default 800)
            height: int
                height of the game window in pixels (default 600)

        """
        ShowBase.__init__(self)

        # We use an Orthographic projection for 2D
        lens = OrthographicLens()

        # And set the film size to work with pixel units,
        # or whatever is appropriate for your scene
        lens.setFilmSize(width, height)

        # Adjust the offset so that bottom-left corner is at (0,0)
        # and top-right at (SCREEN_WIDTH,SCREEN_HEIGHT)
        lens.setFilmOffset(width/2,height/2)

        # Near and far plane values are adjusted to include Y=0
        lens.setNearFar(Game2D.NEAR_VAL, Game2D.FAR_VAL)

        # Updates the camera node of our scence
        base.cam.node().setLens(lens)

        self.gameTask = taskMgr.add(self.topLevelGameLoop, "gameLoop")

        self.width = width
        self.height = height
        
        # The container for all actors that need to be updated in gameLoop
        self.actors = []

    # Used to add an actor to the actors list
    def addActor(self, a):
        self.actors.append(a)
        
    def topLevelGameLoop(self, task):
        """
        This is the top level game loop. Every Game2D game will run this code at each frame.
        This contains all the code that needs to be performed for every Game2D game on each frame.
        Code specific to a particular game should be placed in gameLoop() method of that particular game.

        Parameters
        ----------
            task (Task):

        Returns
        -------
            Several possible values can be returned:

                Game2D.cont : keeps the game running
                Game2D.done : exits the game loop (the game is done).
        """

        # Get the delta time that has elapsed since we were last here
        dt = globalClock.getDt()

        notExpired = []
        # Update all actors
        for a in self.actors:
            if(a.isLifeExpired()):
                a.removeNode()
            else:
                notExpired.append(a)
                a.update(dt)
                
        self.actors = notExpired
            
        # Pass the control to the game gameLoop() method and return whatever is returned from gameLoop()
        return self.gameLoop(task, dt)

    # game loop for a specific game. Derived classes will probably override this to
    # do whatever is needed in a particular game. We simply return Task.cont to signal
    # that the game is to continue running
    def gameLoop(self, task, dt):
        """
        Generic gameLoop() method. In Game2D it simply keeps the game running.
        Derived classes should override this method and implement the game specific game loop.

        Parameters
        ----------
            task (Task):
            dt (): the elapsed time since last call to this method.
        Returns
        -------
            One of the following: Game2D.cont, Game2D.done, Game2D.exit, ...
        """
        return cont
