from gameactor import GameActor
from bullet import Bullet
from panda3d.core import LVector2
from math import sin, cos, pi

class Ship(GameActor):

    ROTATION_SPEED = 200
    MAX_SPEED = 140
    THRUST_STRENGTH = 4
    BULLET_SPEED = 200

    def __init__(self, pos):
        GameActor.__init__(self, "ship.png", pos, LVector2(16,16))

    def rotate(self, ammount):
        # get current adding
        heading = self.getR()
        # compute new value
        heading += ammount
        # set the new value
        self.setR(heading % 360)

    def rotateLeft(self, dt):
        self.rotate(-dt * Ship.ROTATION_SPEED)

    def rotateRight(self, dt):
        self.rotate(dt * Ship.ROTATION_SPEED)
        
    def thrust(self):
        roll = (90 - self.getR()) * pi / 180
        v = LVector2(cos(roll), sin(roll)) * Ship.THRUST_STRENGTH
        
        vel = self.getVelocity()
        vel = vel + v
        self.setVelocity(vel)
        
        
        
    def limitSpeed(self):
        vel = self.getVelocity()
        speed = vel.length()
        if speed > Ship.MAX_SPEED:
            vel.normalize()
            vel = vel * Ship.MAX_SPEED
            self.setVelocity(vel)

    def fire(self, game):
        roll = (90 - self.getR()) * pi / 180
        v = LVector2(cos(roll), sin(roll))
        bullet = Bullet(self.getPosition())
        bullet.setVelocity(v * Ship.BULLET_SPEED + self.getVelocity())
        bullet.setBin("fixed", 41)
        game.addActor(bullet)
                 
       
    def update(self, dt):
        GameActor.update(self, dt)
        self.limitSpeed()
