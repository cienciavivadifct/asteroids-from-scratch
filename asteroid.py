from gameactor import GameActor
from random import random, randint
from math import sin, cos, pi

from panda3d.core import LVector2

class Asteroid(GameActor):
    
    INITIAL_SPEED = 20
    
    def __init__(self, pos):
        GameActor.__init__(self, "asteroid%d.png" % randint(1,3), pos, LVector2(60,60))
        
        heading = random() * 2 * pi
        v = LVector2(sin(heading), cos(heading)) * Asteroid.INITIAL_SPEED
        self.setVelocity(v)